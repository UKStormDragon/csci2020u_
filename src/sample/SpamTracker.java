//GIT reverted all of my code without me telling it too, destroying all my comments and costing me a lot of my improvement. It does not push and the commits randomly just dissapeared and I have no idea why
//I'm redoing all my comments and getting it back up to completion but all of the extra research and implmentation i did is gone.
package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.Key;
import java.util.*;

/**
 * Created by ukstormdragon on 09/03/16.
 */
public class SpamTracker {
    private Map<String,Integer> wordCountSpam;
    private Map<String,Integer> wordCountHam;
    private Map<String,Integer> emailCount;
    private int emailsSpam;
    private int emailsHam;
    private Map<String,Float> spamFrequency;
    private Map<String,Float> hamFrequency;
    private Map<String,Float> finalFrequency;
    private Map<String,Float> trainTest;
    public ArrayList<TestFile> list;
    int i = 0;

    SpamTracker() {
        wordCountSpam = new TreeMap<>();
        wordCountHam = new TreeMap<>();
        emailCount = new TreeMap<>();  //Used to store words specific for the email.
        spamFrequency = new TreeMap<>();  //Ratio for spam.
        hamFrequency = new TreeMap<>(); //Ratio for Ham
        finalFrequency = new TreeMap<>();   //Combined ratio.
        trainTest = new TreeMap<>();
        list = new ArrayList<>();
        emailsHam = 0;
        emailsSpam = 0;

    }


    public void trainTrackerSpam(File file) throws Exception{



        try {
           processFileSpam(file);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }
    public void trainTrackerHam(File file) throws Exception{



        try {
            processFileHam(file);

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public void testTracker(File file1, File file2) throws Exception{
        try{
            scanSpam(file1);
            scanHam(file2);
        }
        catch(FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    //Scan Spam Files.
    public void scanSpam(File file) throws Exception{

        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {
                scanSpam(filesInDir[i]);
            }
        } else if (file.exists()) {
            // load all of the data,R and process it into words
            Scanner scanner = new Scanner(file);
            String name = file.getName();
            emailCount.clear();
            while (scanner.hasNext()) {
                String word = scanner.next();
                if (checkWord(word)) {
                    countWord(word);
                }

            }
            filestuff(name, "Spam"); //PAss name and Catagory.
        }

    }

    public void filestuff(String eName, String eType)
    {
       // System.out.println(eName +  " " + eType);

        Set<String> keys = emailCount.keySet();     //Iterator through map.
        Iterator<String> KeyIterator = keys.iterator();
        float chance = 0;
        double newThing=0;

        while(KeyIterator.hasNext())
        {
            String key = KeyIterator.next();

            if(finalFrequency.containsKey(key))   //MAke sure word is contained first.
            {
                double thing = finalFrequency.get(key);

                newThing += (Math.log(1- thing)) - Math.log(thing);
                    //System.out.println(thing);



            }



        }

        double otherthing = 1/(1+Math.pow(Math.E,newThing));
        //otherthing = otherthing/100;
        chance += otherthing;
        list.add(i,new TestFile(eName,chance, eType));

        //System.out.println(list.get(i).getFilename() + " " + list.get(i).getSpamProbability() + " "  + list.get(i).getActualClass());
        i++;

    }

    public void scanHam(File file)throws Exception{
        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {
                scanHam(filesInDir[i]);
            }
        } else if (file.exists()) {
            // load all of the data,R and process it into words
            Scanner scanner = new Scanner(file);
            String name = file.getName();
            emailCount.clear();     //Clear the email map.
            while (scanner.hasNext()) {
                String word = scanner.next();
                if (checkWord(word)) {          //check to make sure the word is a word.
                    countWord(word);            //Put the word in the word map.
                }

            }
            filestuff(name, "Ham");
        }
    }
    public void processFileSpam(File file) throws Exception
    {

       // System.out.println("TESTING");
        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {
                processFileSpam(filesInDir[i]);
            }
        } else if (file.exists()) {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);
            emailsSpam+=1;
            emailCount.clear();
            while (scanner.hasNext()) {
                String word = scanner.next();
                if (checkWord(word)) {
                    countWordSpam(word);
                }
            }
        }
    }

    public void processFileHam(File file) throws Exception
    {
        // System.out.println("TESTING");
        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {
                processFileHam(filesInDir[i]);
            }
        } else if (file.exists()) {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);
            emailCount.clear();
            emailsHam+=1;
            while (scanner.hasNext()) {
                String word = scanner.next();
                if (checkWord(word)) {
                    countWordHam(word);
                }
            }
        }
    }

    public boolean checkWord(String word){
        String tempWord = "^[a-zA-Z]*$";
        if (word.matches(tempWord)){
            return true;
        }
        else{
            return false;
        }

    }

    public void countWordSpam(String word)
    {
        if(!emailCount.containsKey(word)){ //Make sure that the email map has not already got the word stored.
            if(wordCountSpam.containsKey(word))
            {
                wordCountSpam.put(word, wordCountSpam.get(word)+1);
               // System.out.println(word + " " + wordCountSpam.get(word));

            }
            else{
                wordCountSpam.put(word,1);   //Put word in spam map.

            }
            emailCount.put(word,1);
        }

    }
    public void countWordHam(String word)
    {
        if(!emailCount.containsKey(word)){
        if(wordCountHam.containsKey(word))
        {
            wordCountHam.put(word, wordCountHam.get(word)+1);
           // System.out.println(word + " " + wordCountHam.get(word));
        }
        else{
            wordCountHam.put(word,1);
        }
            emailCount.put(word,1);
        }

    }

    public void countWord(String word)
    {
        if(!emailCount.containsKey(word)) {

            emailCount.put(word, 1);
        }
    }

    public void printWordCountSpam(int minCount, File outputFile) throws FileNotFoundException {
        System.out.println("Saving word counts to " + outputFile.getAbsolutePath());
        if (!outputFile.exists() || outputFile.canWrite()) {
            PrintWriter fout = new PrintWriter(outputFile);

            Set<String> keys = wordCountSpam.keySet();
            Iterator<String> keyIterator = keys.iterator();

            while(keyIterator.hasNext()) {
                String key = keyIterator.next();
                int count = wordCountSpam.get(key);

                if (count >= minCount) {
                    //System.out.println(key + ": " + count);
                    fout.println(key + ": " + count);
                }
            }
            fout.close();
        } else {
            System.err.println("Cannot write to output file");
        }
    }
    public void printWordCountHam(int minCount, File outputFile) throws FileNotFoundException {
        System.out.println("Saving word counts to " + outputFile.getAbsolutePath());
        if (!outputFile.exists() || outputFile.canWrite()) {
            PrintWriter fout = new PrintWriter(outputFile);

            Set<String> keys = wordCountHam.keySet();
            Iterator<String> keyIterator = keys.iterator();

            while(keyIterator.hasNext()) {
                String key = keyIterator.next();
                int count = wordCountHam.get(key);

                if (count >= minCount) {
                    //System.out.println(key + ": " + count);
                    fout.println(key + ": " + count);
                }
            }
            fout.close();
        } else {
            System.err.println("Cannot write to output file");
        }
    }

//Calculate spam ratio.
    public void calulcateSpam()
    {
        Set<String> keys = wordCountSpam.keySet();
        Iterator<String> keyIterator = keys.iterator();

        while(keyIterator.hasNext()) {
            String key = keyIterator.next();
            int count = wordCountSpam.get(key);

            if (count >= 2) {

                float tempI = count/emailsSpam;
                spamFrequency.put(key,tempI);
                //System.out.println(key + " " + count);
                //spamFrequency
            }
        }

    }

    //Calculate Ham Ratio.
    public void calulcateHam()
    {
        Set<String> keys = wordCountHam.keySet();
        Iterator<String> keyIterator = keys.iterator();

        while(keyIterator.hasNext()) {
            String key = keyIterator.next();
            int count = wordCountHam.get(key);

            if (count >= 2) {

                float tempI = count/emailsHam;
                hamFrequency.put(key,tempI);
                //System.out.println(key + " " + count);
                //spamFrequency
            }
        }

    }

    //Calculate final ratio.
    public void calculate()
    {
        Set<String> keys = wordCountHam.keySet();
        Iterator<String> KeyIterator = keys.iterator();

        while(KeyIterator.hasNext())
        {
            String key = KeyIterator.next();
            float countH = wordCountHam.get(key);
            float countS = 0.0f;
            if(wordCountSpam.containsKey(key))
            {

            countS = wordCountSpam.get(key);
            }

            float math = countS/(countS + countH);
            System.out.println(math);
            finalFrequency.put(key,math);

        }
    }
    public void printFrequency(int minCount, File outputFile) throws FileNotFoundException {
        System.out.println("Saving word counts to " + outputFile.getAbsolutePath());
        if (!outputFile.exists() || outputFile.canWrite()) {
            PrintWriter fout = new PrintWriter(outputFile);

            Set<String> keys = finalFrequency.keySet();
            Iterator<String> keyIterator = keys.iterator();

            while(keyIterator.hasNext()) {
                String key = keyIterator.next();
                float count = finalFrequency.get(key);

                if (count >= minCount) {
                    //System.out.println(key + ": " + count);
                    fout.println(key + ": " + count);
                }
            }
            fout.close();
        } else {
            System.err.println("Cannot write to output file");
        }
    }
float accuracyCount = 0;
    float totalCount=0;
    float Accuracy =0;
    float precision =0;
    float spamCounter=0;
    float totalSpam=0;
    public ObservableList<TestFile> getAllFiles() {

        ObservableList<TestFile> files = FXCollections.observableArrayList();

        for (int t = 0; t < list.size(); t++) {

            if (list.get(t).getSpamProbability() > .50 && list.get(t).getActualClass() == "Spam")
            {
                accuracyCount++;
                spamCounter++;
            }
            else if (list.get(t).getActualClass() == "Ham" && list.get(t).getSpamProbability() <.50)
            {
                accuracyCount++;

            }
            if(list.get(t).getActualClass() == "Spam")
            {
                totalSpam++;
            }
            totalCount++;


            files.add(t, list.get(t));
        }
        //}
        Accuracy = accuracyCount/totalCount;
        precision = spamCounter/totalSpam;

        return files;

    }
}
