package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.*;
import javafx.scene.input.KeyCombination;
import javafx.scene.image.*;
import javafx.collections.*;
import javafx.event.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

public class Main extends Application {
    private Stage window;
    private BorderPane layout;
    private TableView<TestFile> table;


    @Override
    public void start(Stage primaryStage) throws Exception {
        program(primaryStage);
       //reference(primaryStage);
    }

    private void program(Stage primaryStage) throws Exception{
        primaryStage.setTitle("JavaFX Demo");

        /* create the menu (for the top of the user interface) */
        Menu fileMenu = new Menu("File");
        MenuItem newMenuItem = new MenuItem("New", imageFile("images/new.png"));
        newMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        fileMenu.getItems().add(newMenuItem);
        fileMenu.getItems().add(new SeparatorMenuItem());
        fileMenu.getItems().add(new MenuItem("Open...", imageFile("images/open.png")));
        fileMenu.getItems().add(new SeparatorMenuItem());
        fileMenu.getItems().add(new MenuItem("Save", imageFile("images/save.png")));
        fileMenu.getItems().add(new MenuItem("Save As...", imageFile("images/save_as.png")));
        fileMenu.getItems().add(new SeparatorMenuItem());
        MenuItem exitMenuItem = new MenuItem("Exit", imageFile("images/exit.png"));
        fileMenu.getItems().add(exitMenuItem);
        exitMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+Q"));
        exitMenuItem.setOnAction( e -> System.exit(0) );

        Menu editMenu = new Menu("Edit");
        editMenu.getItems().add(new MenuItem("Cut", imageFile("images/cut.png")));
        editMenu.getItems().add(new MenuItem("Copy", imageFile("images/copy.png")));
        editMenu.getItems().add(new MenuItem("Paste", imageFile("images/paste.png")));

        Menu helpMenu = new Menu("Help");
        helpMenu.getItems().add(new MenuItem("About...", imageFile("images/about.png")));
        helpMenu.getItems().add(new SeparatorMenuItem());
        helpMenu.getItems().add(new MenuItem("Help...", imageFile("images/help.png")));

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().add(fileMenu);
        menuBar.getMenus().add(editMenu);
        menuBar.getMenus().add(helpMenu);

        /* create the table (for the center of the user interface) */


        /* create the table's columns */


        //Get Directory.

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File("."));
        File mainDirectory = directoryChooser.showDialog(primaryStage);


        //Set Paths

        File testSpamDirectory = new File(mainDirectory.getPath() + "/test/spam/");
        File testHamDirectory = new File(mainDirectory.getPath() + "/test/ham/");
        File trainSpamDirectory = new File(mainDirectory.getPath() + "/train/spam/");
        File trainHamDirectory = new File(mainDirectory.getPath() + "/train/ham/");

        SpamTracker SpamBot300 = new SpamTracker();
        try {
            SpamBot300.trainTrackerSpam(trainSpamDirectory);        //Train Spam
            SpamBot300.printWordCountSpam(2, new File("src/spamMap"));      //output to map.
            SpamBot300.trainTrackerHam(trainHamDirectory);              //Train Ham
            SpamBot300.printWordCountHam(2, new File("src/hamMap"));        //output to map.
            SpamBot300.calulcateSpam();                 //Calculate Spam Ratio
            SpamBot300.calulcateHam();                  //Calculate Ham Ratio
            SpamBot300.calculate();                     ///Calculate Final ratiol
            SpamBot300.printFrequency(0,new File("src/Frequency"));     //Output to map.
            SpamBot300.testTracker(testSpamDirectory,testHamDirectory);     //Test Bot

        }
        catch(IOException e){
            e.printStackTrace();
        }


        table = new TableView<>();
        table.setItems(SpamBot300.getAllFiles());
        table.setEditable(true);

          /* create the table's columns */
        TableColumn<TestFile,String> idColumn = null;
        idColumn = new TableColumn<>("filename");
        idColumn.setMinWidth(100);
        idColumn.setCellValueFactory(new PropertyValueFactory<>("filename"));

        TableColumn<TestFile,Float> spamColumn = null;
        spamColumn = new TableColumn<>("spamProbability");
        spamColumn.setMinWidth(200);
        spamColumn.setCellValueFactory(new PropertyValueFactory<>("spamProbability"));


        TableColumn<TestFile,Float> classColumn = null;
        classColumn = new TableColumn<>("actualClass");
        classColumn.setMinWidth(200);
        classColumn.setCellValueFactory(new PropertyValueFactory<>("actualClass"));

        table.getColumns().add(idColumn);
        table.getColumns().add(spamColumn);
        table.getColumns().add(classColumn);

        GridPane editArea = new GridPane();
        editArea.setPadding(new Insets(10, 10, 10, 10));
        editArea.setVgap(10);
        editArea.setHgap(10);

        String temp = String.valueOf(SpamBot300.Accuracy);
        Label sidLabel = new Label("ACCURACY:");
        editArea.add(sidLabel, 0, 0);
        Label sidField = new Label(temp);

        editArea.add(sidField, 1, 0);

        temp = String.valueOf(SpamBot300.precision);
        Label aLabel = new Label("PRECISION:");
        editArea.add(aLabel, 0, 1);
        Label aField = new Label(temp);

        editArea.add(aField, 1, 1);


        layout = new BorderPane();
        layout.setTop(menuBar);
        layout.setCenter(table);
        layout.setBottom(editArea);

        Scene scene = new Scene(layout, 600, 600);
        primaryStage.setScene(scene);
        primaryStage.show();

    }







    private ImageView imageFile(String filename) {
        return new ImageView(new Image("file:"+filename));
    }

    public static void main(String[] args) {
        launch(args);
    }
}
